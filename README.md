# netreboot
A small Raspberry Pi-based internet rebooter.

This script runs as a service that checks internet connection status and restarts modem/router in correct order.
