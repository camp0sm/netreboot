#!/usr/bin/python3

import sys
import requests
from time import sleep
try:
    import RPi.GPIO as GPIO
except:
    #     
    class Dummy():
        def __init__ (self): print("using dummy gpio")
        def dummy (self, *args, **kwargs): pass
        def __getattr__ (self, name): return self.dummy
    GPIO = Dummy()
        

class NetRebooter():
    #
    TEST_POINT_TIMEOUT = 5              # [s] per site; timeout
    GOOD_CONNECTION_INTERVAL = 60*10    # [s] test interval when online
    BAD_CONNECTION_INTERVAL = 60*5      # [s] test interval when offline
        
    DEVICE_POWER_DELAY = 20             # [s] interval after command
    
    MAX_ATTEMPTS_ALL_FAILED = 3         # number of attemps before
                                        # declaring offline
                                        
    MODEM_CONTROL_PIN = 13
    ROUTER_CONTROL_PIN  = 15

    
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0)'
                      'Gecko/20100101 Firefox/57.0',
    }

    testPoints = [
        'https://core.telegram.org',
        'https://www.google.com',
        'https://www.reddit.com',
        'https://www.santander.com.br',
        'https://jovemnerd.com.br'
    ]
    
    def __init__ (self):
        #
        self.remainingAttempts = 0
        
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.MODEM_CONTROL_PIN, GPIO.OUT)
        GPIO.setup(self.ROUTER_CONTROL_PIN, GPIO.OUT)
        
        GPIO.output(self.MODEM_CONTROL_PIN, GPIO.LOW)
        GPIO.output(self.ROUTER_CONTROL_PIN, GPIO.LOW)
    
    def cleanup (self):
        GPIO.output(self.MODEM_CONTROL_PIN, GPIO.LOW)
        GPIO.output(self.ROUTER_CONTROL_PIN, GPIO.LOW)
        GPIO.cleanup()
    
    def resetRemainingAttempts (self):
        self.remainingAttempts = self.MAX_ATTEMPTS_ALL_FAILED   
    
    def countAllFailed (self):
        self.remainingAttempts = max(0, self.remainingAttempts - 1)
    
    ####################################################################
    def countdownDelay (self, msg, delay):
        delay = int(delay)
        
        while delay:
            tmin, tsec = divmod(delay, 60)
            sys.stdout.write("\r{} {}m{:02d}s.. ".format(msg, tmin, tsec))
            sys.stdout.flush()
            delay -= 1
            sleep(1.0)
        sys.stdout.write("\r" + ' '*80 + '\n')
        sys.stdout.flush()
    
    def testConnection (self):
        for i, tp in enumerate(self.testPoints):
            print(
                "> testing {} ({}/{}).. ".format(tp, i+1, len(self.testPoints)), 
                end='')
                
            try:
                r = requests.get(
                    tp, 
                    timeout=self.TEST_POINT_TIMEOUT, 
                    headers=self.headers)
                    
                if r.ok:
                    print("success!")
                    self.resetRemainingAttempts()
                    return True
            except:
                print("fail!")
        #
        return False

    def performReset (self):
        #
        print("turning router off..")
        GPIO.output(self.ROUTER_CONTROL_PIN, GPIO.HIGH)
        
        print("turning modem off..")        
        GPIO.output(self.MODEM_CONTROL_PIN, GPIO.HIGH)
        
        self.countdownDelay("waiting", self.DEVICE_POWER_DELAY)
        
        print("turning modem on..")
        GPIO.output(self.MODEM_CONTROL_PIN, GPIO.LOW)
        self.countdownDelay("waiting", self.DEVICE_POWER_DELAY)
        
        print("turning router on..")
        GPIO.output(self.ROUTER_CONTROL_PIN, GPIO.LOW)
        self.countdownDelay("waiting", self.DEVICE_POWER_DELAY)
    
    
    def loop (self):
        self.resetRemainingAttempts()
        
        while True:
            # test if at least one point connects
            # attempt counter will reset if so
            if self.testConnection():
                print("system is connected!")
                self.countdownDelay("testing again in", self.GOOD_CONNECTION_INTERVAL)
            else:
                # no point connected
                self.countAllFailed()
                if self.remainingAttempts:
                    print(
                        "system is NOT connected (attempt {}/{})".format(
                        self.MAX_ATTEMPTS_ALL_FAILED - self.remainingAttempts,
                        self.MAX_ATTEMPTS_ALL_FAILED)
                    )
                    
                    self.countdownDelay("testing again in", self.BAD_CONNECTION_INTERVAL)
                else:
                    print("system is NOT connected (attempt {}/{})\n".format(
                        self.MAX_ATTEMPTS_ALL_FAILED - self.remainingAttempts,
                        self.MAX_ATTEMPTS_ALL_FAILED)
                    )
                    
                    print("preparing to restart modem and router..")
                    self.performReset()
                    self.resetRemainingAttempts()

                    self.countdownDelay("testing again in", self.BAD_CONNECTION_INTERVAL)
        
if __name__ == '__main__':
    #
    def main():
        NR = NetRebooter()
        try:
            NR.loop()
        except Exception as e:
            print("error:", e)
            NR.cleanup()

    # change process name 
    import ctypes
    libc = ctypes.cdll.LoadLibrary('libc.so.6')
    libc.prctl(15, ctypes.c_char_p(b'NetRebooter'), 0, 0, 0)
       
    main()
    
